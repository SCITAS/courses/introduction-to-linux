SHELL := /bin/bash

slides: build-slides clean-slides

build-slides:
	pdflatex slides.tex
	xdg-open slides.pdf

clean-slides:
	rm slides.{aux,log,nav,out,snm,toc,vrb}

view: slides.pdf
	impressive --background white -c persistent --nologo --windowed -g 1920x1080 slides.pdf

ex: build-ex clean-ex

build-ex:
	pdflatex exercise_sheet.tex
	xdg-open exercise_sheet.pdf

clean-ex:
	rm exercise_sheet.{aux,log,out}
